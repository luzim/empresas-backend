/**
 * EnterpriseController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    getAll: async(req,res) =>{
        let { enterprise_types,name } = req.allParams();
        if(enterprise_types || name){
            try{
                //única solução que encontrei para a parte de case insensitive no POSTGRESQL 
                let enterprises = await sails.sendNativeQuery(`
                    SELECT 
                    * 
                FROM 
                    enterprise 
                WHERE 
                    ${enterprise_types ? 'enterprise_type = ' + enterprise_types : ''}
                    ${(name && enterprise_types) ? ' AND ': '' }
                    ${name ? 'LOWER(enterprise_name) LIKE LOWER(' +'\'%' + name + '%\'' + ')' : ''}
                    `
                , []);
                enterprises = enterprises.rows
                if(enterprises.length){
                    for(let i = 0;i<enterprises.length;i++){
                        let enterprise = enterprises[i];
                        let type = await EnterpriseType.findOne({id: enterprise.enterprise_type});
                        enterprise.enterprise_type = {id: type.id, enterprise_type_name: type.enterprise_type_name};
                        delete enterprise.createdAt;
                        delete enterprise.updatedAt;
                    }
                    return res.status(200).json({enterprises: enterprises});    
                }else{
                    return res.status(404).json({message: 'Nenhuma empresa encontrada'});    
                }
            }catch(error){
                return res.status(500).json({message: 'Internal server error'});
            }
        }else{
            try{
                let enterprises = await Enterprise.find();
                if(enterprises.length){
                    for(let i = 0;i<enterprises.length;i++){
                        let enterprise = enterprises[i];
                        let type = await EnterpriseType.findOne({id: enterprise.enterprise_type});
                        enterprise.enterprise_type = {id: type.id, enterprise_type_name: type.enterprise_type_name};
                        delete enterprise.createdAt;
                        delete enterprise.updatedAt;
                    }
                    return res.status(200).json({enterprises: enterprises});    
                }else{
                    return res.status(404).json({message: 'Nenhuma empresa encontrada'});    
                }
            }catch(error){
                return res.status(500).json({message: 'Internal server error'});
            }
        }
    },
    get: async(req,res) =>{
        try{
            let { id } = req.allParams();
            if(id){
                let enterprise = await Enterprise.findOne({id});
                if(enterprise){
                    let type = await EnterpriseType.findOne({id: enterprise.enterprise_type});
                    enterprise.enterprise_type = {id: type.id, enterprise_type_name: type.enterprise_type_name};
                    delete enterprise.createdAt;
                    delete enterprise.updatedAt;
                    
                    return res.status(200).json({enterprise: enterprise, success: true});    
                }else{
                    return res.status(404).json({message: 'Nenhuma empresa encontrada'});    
                }
            }else{
                return res.status(400).json({message: 'Nenhum parâmetro foi dado'})
            }
            
        }catch(error){
            return res.status(500).json({message: 'Internal server error'});
        }
    },
    createMany: async(req,res) => {
        let { enterprises } = req.allParams();
        if(enterprises){
            try{
                let count = 0;
                for(let i = 0;i<enterprises.length;i++){
                    let enterprise = enterprises[i];
                    delete enterprise.id;
                    let enterpriseExists = await Enterprise.findOne({enterprise_name: enterprise.enterprise_name});
                    if(!enterpriseExists){
                        enterpriseType = enterprise.enterprise_type;
                        delete enterprise.enterprise_type;
                        let enterpriseTypeName = await EnterpriseType.findOne({enterprise_type_name: enterpriseType.enterprise_type_name});
                        if(!enterpriseTypeName){
                            enterpriseTypeName = await EnterpriseType.create({enterprise_type_name: enterpriseType.enterprise_type_name}).fetch();
                        }
                        enterprise.enterprise_type = enterpriseTypeName.id;
                        await Enterprise.create(enterprise).fetch();
                        count++;
                    }
                }
                return res.status(201).json({message: count + ' Empresas foram criadas de ' + enterprises.length})
            }catch(error){
                return res.status(500).json({message: 'Internal server error'})
            }
        }
        else{
            return res.status(400).json({message: 'Envie pelo menos 1 empresa'})
        }
    }
};

