/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    getUser: async(req,res) => {
        try{
            const user = await User.findOne({id: req.session.user.id});
            if(user){
                return res.status(200).json(user);
            }else{
                return res.status(404).json({message: 'Usuário não encontrado ou inexistente'});    
            }
        }catch(error){
            return res.status(500).json({message: 'Internal server error'});
        }
    },
    singIn: async(req,res) => {
        const { email, password } = req.allParams();
        if(email && password){
            try{
                let user = await User.findOne({email: email});
                if(!user){
                    return res.status(404).json({message: 'Usuário não encontrado'});
                }else{
                    const passwordDecoded = JwtService.verify(
                        user.password,
                        (error, decoded) => {
                          if (error) {
                            throw error;
                          }
                          return decoded.password;
                        },
                    );
                    if(password == passwordDecoded){
                        delete user.password;
                        delete user.token;
                        
                        const token = JwtService.login({...user});
                        let hash = (len, arr) => { 
                            var ans = ''; 
                            for (var i = len; i > 0; i--) { 
                                ans +=  
                                  arr[Math.floor(Math.random() * arr.length)]; 
                            } 
                            return ans; 
                          };
                        let hashcode = hash(22, '1234567890abcdefghijklmnABCDEFGHIJKLMNOPQRSTU');
                        let hashcode1 = hash(22, '1234567890abcdefghijklmnABCDEFGHIJKLMNOPQRSTU');
                        let auth = await Auth.destroyOne({uid: email});
                        await Auth.create({uid: email, client: hashcode1, accessToken: hashcode, token});
                        //await User.updateOne({ id: user.id }).set({ token: token });
                        res.setHeader('access-token', hashcode);
                        res.setHeader('token-type', 'Bearer');
                        res.setHeader('client', hashcode1);
                        res.setHeader('expiry', auth.createdAt + 300);
                        res.setHeader('uid', email);
                        //TAMBÉM RETORNAR ENTERPRISE QUE O USUÁRIO PERTENCE
                        //!!!!!!!!!!!
                        let response = res.status(200).json({investor:user, enterprise: null ,success: true});
                        return response;
                    }
                    return res.status(400).json({ message: 'email ou senha inválidos' });
                }
            }catch(error){
                console.error(error);
                return res.status(500).json({message: error.message});
            }
            
        }else{
            return res.status(400).json({message: 'Parâmetros não encontrados: email ou password'});
        }
    }
};

