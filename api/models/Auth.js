/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
      accessToken: {
        type: 'string',
        allowNull: false
      },
      client: {
        type: 'string',
        allowNull: false  
      },
      uid: {
        type: 'string',
      },
      token: {
        type: 'string',
        allowNull: false
      },
    },
  
  };
  
  