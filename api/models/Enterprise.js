/**
 * Enterprise.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    email_enterprise: {
      type: 'string',
      allowNull: true
    },
    facebook: {
      type: 'string',
      allowNull: true
    },
    twitter: {
      type: 'string',
      allowNull: true
    },
    linkedin: {
      type: 'string',
      allowNull: true
    },
    phone: {
      type: 'string',
      allowNull: true
    },
    own_enterprise: {
      type: 'boolean',
    },
    enterprise_name: {
      type: 'string',
      allowNull: false
    },
    photo: {
      type: 'string',
      allowNull: true
    },
    description: {
      type: 'string',
      allowNull: true
    },
    city: {
      type: 'string',
      allowNull: true
    },
    country: {
      type: 'string',
      allowNull: true
    },
    value: {
      type: 'number',
      defaultsTo: 0
    },
    share_price:{
      type: 'number',
      defaultsTo: 5000.0
    },
    enterprise_type: {
        type: 'number',
        allowNull: false
    }
  },

};

