/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    investor_name: {
      type: 'string',
    },
    email: {
      type: 'string',
      allowNull: null
    },
    password: {
      type: 'string',
      allowNull: false  
    },
    city: {
      type: 'string',
    },
    country: {
      type: 'string',
    },
    balance: {
      type: 'number',
    },
    photo: {
      type: 'string',
    },
    portfolio: {
      type: 'number',
    },
    portfolio_value: {
      type: 'number',
      defaultsTo: 0
    },
    first_access: {
      type: 'boolean',
      defaultsTo: true
    },
    super_angel: {
      type: 'boolean',
      defaultsTo: false
    },
  },

};

