module.exports = async (req, res, next) => {
    let token;
    let auth;
    if (req.headers && req.headers['access-token'] && req.headers.client && req.headers.uid ) {
      accessToken = req.headers['access-token'];
      client = req.headers.client;
      uid = req.headers.uid;
      auth = await Auth.findOne({uid, accessToken, client });
      if( auth ) token = auth.token;
    }
    else if (req.param('token')) {
      token = req.param('token');
  
      delete req.query.token;
    } else {
      return res.status(401).json('No Authorization header found.');
    }
    JwtService.verify(token, (err, decoded) => {
      if (err) return res.status(401).json('invalid-token');
      //req.session.token = token;
      try {
          User.findOne({
              id: decoded.id
          }).exec((err, result) => {
              if (err) return res.status(400).json(err);
              if (!result){
                return res.status(404).json('user-not-found');
              } 
              if(auth){
                res.setHeader('access-token', auth.accessToken);
                res.setHeader('token-type', 'Bearer');
                res.setHeader('client', auth.client);
                res.setHeader('expiry', auth.createdAt + 300);
                res.setHeader('uid', auth.uid);
              }
              req.session.user = result;

              next();
          });
      
      } catch (error) {
          console.error(error);
          res.status(400).json({
              code: error.code,
              details: error.details
          });
      }

    });    

}