const jwt = require('jsonwebtoken');
const jwtSecret = sails.config.secrets.jwtSecret;
const algorithm = sails.config.secrets.algorithm;

module.exports = {
  issue: (payload) => {
    const token = jwt.sign(payload, jwtSecret, { algorithm });
    return token;
  },

  login: (payload) => {
    const token = jwt.sign(payload, jwtSecret, { algorithm, expiresIn: 300 });
    return token;
  },

  verify: (token, callback) => {
    return jwt.verify(token, jwtSecret,callback);
  },
};
