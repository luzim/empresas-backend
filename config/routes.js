/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  'POST /api/v1/users/singUp': {
    controller: 'UserController',
    action: 'singUp',
  },
  'POST /api/v1/users/auth/sign_in': {
    controller: 'UserController',
    action: 'singIn',
  },
  'POST /api/v1/users/auth/Oauth/sign_in': {
    controller: 'UserController',
    action: 'singInOauth',
  },

  'GET /api/v1/users': {
    controller: 'UserController',
    action: 'getUser',
  },
  'POST /api/v1/enterprises/createMany': {
    controller: 'EnterpriseController',
    action: 'createMany',
  },
  'GET /api/v1/enterprises': {
    controller: 'EnterpriseController',
    action: 'getAll',
  },
  'GET /api/v1/enterprises/:id': {
    controller: 'EnterpriseController',
    action: 'get',
  },
  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
